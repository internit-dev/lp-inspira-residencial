<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Inspira Residencial - 2 e 3 quartos na Tijuca</title>

  <meta name="description" content="none">
  <meta name="robots" content="all">
  <meta name="author" content="none">
  <meta name="keywords" content="none">

  <meta property="og:type" content="page">
  <meta property="og:url" content="none">
  <meta property="og:title" content="none">
  <meta property="og:image" content="none">
  <meta property="og:description" content="Apartamentos 2 e 3 quartos na Tijuca com suíte e varanda gourmet em todas unidades e exclusivo lazer no Rooftop com piscina, churraqueira e mais!">
  
  <meta property="article:author" content="none">

  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@none">
  <meta name="twitter:title" content="none">
  <meta name="twitter:creator" content="@none">
  <meta name="twitter:description" content="none">
  <link rel="shortcut icon" href="./assets/src/img/favicon.png" type="image/x-icon">
  <!-- Style -->
  <link rel="stylesheet" href="./assets/dist/css/template-globals.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

  <!-- Jquery -->
  <script src="./assets/dist/js/libary/jquery/jquery-3.6.0.min.js"></script>

   <!-- INTEGRAÇÃO HEADER -->
  <?php require_once 'integracao/integracaoHead.php'; ?>

</head>

<header>
  <nav>
    
  </nav>
</header>

<body>