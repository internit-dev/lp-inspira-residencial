<?php

return [
         'geral' => [
            'status' => false,
            'idTagManager' => 'GTM-WMBLMJK',
            'idAnalytics' => 'UA-41053658-16',
            
        ],
         'principal' => [
            'status' => false,
            'event' => '',
            'event_category' => '',
            'event_label' => '',
            
        ],
         'whatsapp' => [
            'status' => false,
            'event' => '',
            'event_category' => '',
            'event_label' => '',
            
        ],
    
];    