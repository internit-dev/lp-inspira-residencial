<?php

/**
 * The main template file
 *
 * Este é o modelo de landing page base da internit, desenvolvedora web. 
 * Nele você encontrará recursos para construir totalmente uma landing page 
 * sem precisar baixar nada.
 *
 * @package internit
 */

require './header.php';

?>

<main id="primary" class="site-main">


  <section class="main-section">
    <div class="left-sections">
      <div class="container">
        <div class="logo-container">
          <img src="./assets/src/img/Logo.png" alt="logo">
          <p>VIVA À ALTURA DOS SEUS SONHOS</p>
        </div>

        <div class="middle-container">
          <div class="text-container">
            <p><span>2, 3 e 4 QUARTOS NO MELHOR DA TIJUCA</span><br>
              + Coberturas Duplex e Apartamentos Up-Garden
            </p>
          </div>

          <div class="text-container">
            <p>
              <span>
                VARANDA GOURMET INTEGRADA</span><br> em todas as unidades
            </p>
          </div>

          <div class="text-container">
            <p>
              <span>LAZER NO ROOFTOP</span> com Piscina na cobertura
            </p>
          </div>

          <div class="text-container">
            <p>
              <span>
              RUA DR ANÍBAL MOREIRA, 135 
              </span><br>
              (trecho inicial da Rua Carvalho Alvim)
            </p>
          </div>

          <div class="form-container">
            <?php ############################################################################################################# 
            ?>
            <?php $formName = 'principal'; ?>
            <form action="" name="<?= $formName ?>" id="form-<?= $formName ?>">

              <h3>Entre em contato e resgate o seu prêmio!</h3>
              <div class="form-group">
                <input type="name" name="Nome" class="form-control" id="text" aria-describedby="name" placeholder="Nome">
              </div>

              <!-- input email -->
              <div class="form-group">
                <input name="Email" type="email" class="form-control" placeholder="E-mail*" required>
              </div>


              <!-- input telefone -->
              <div class="form-group">
                <input type="tel" name="Telefone" class="form-control js-input-celular" id="tel" aria-describedby="tel" required>

              </div>


              <!-- input checkbox -->
              <p>
                <input name="Termos" type="checkbox" value="aceito" required>Li e aceito os termos e
                <a href="" data-toggle="modal" data-target="#modalpolitica">Política de Privacidade</a>.
              </p>

              <!-- botão enviar -->
              <div class="submit-button">
                <button id="form-botao-<?= $formName ?>" type="submit" class="">Solicitar mais informações</button>
              </div>

              <div class="text-center submit-message font-weight-bold mt-3"></div>

            </form>
            <?php unset($formName); ?>
            <?php ############################################################################################################# 
            ?>
          </div>
          <div class="last-section">
            <p>Realização: Prodomo e Leblon Realty</p>
          </div>

        </div>


      </div>
      <div class="mouse_scroll">
        <div class="mouse">
          <div class="wheel"></div>
        </div>
        <div class="itens_mouse">
          <span class="m_scroll_arrows unu"></span>
          <span class="m_scroll_arrows doi"></span>
          <span class="m_scroll_arrows trei"></span>
        </div>
      </div>
      <div id="swiperDesktop" class="swiper-pagination"></div>
      <a href="./assets/pdf/BOOK_INSPIRA.pdf" download>
        <div class="book-container">
          <p>Veja o book de vendas completo</p>
        </div>
      </a>
      <a data-fslightbox="html5-youtube-videos" href="https://www.youtube.com/watch?v=8K1C_EXjAWo" class="video-link">
        <div class="video-container">
          <i class="fas fa-video"></i>
          <p>Assista a nossa Inspiração</p>
        </div>
      </a>
    </div>
    <div class="right-sections">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <div class="swiper-slide"><img src="./assets/src/img/Banner-new-inspira3.png" alt="">
            <!-- <p>Salão de Festas</p> -->
          </div>
          <div class="swiper-slide"><img src="./assets/src/img/Banner-new-inspira1.png" alt="">
            <!-- <p>Fachada</p> -->
          </div>
          <div class="swiper-slide"><img src="./assets/src/img/Banner-new-inspira2.png" alt="">
            <!-- <p>Piscina</p> -->
          </div>
        </div>
        <!-- Add Pagination -->

      </div>
    </div>

    <!-- MOBILE CONTAINER ONLYA -->
    <div class="mobile-plus-section">
      <div class="pagination">
        <div id="SwiperMobile" class=""></div>
      </div>
      <div class="form-container">
        <?php ############################################################################################################# 
        ?>
        <?php $formName = 'principal'; ?>
        <form action="" name="<?= $formName ?>" id="form-<?= $formName ?>">

          <h3>Entre em contato e resgate o seu prêmio!</h3>
          <div class="form-group">
            <input type="name" name="Nome" class="form-control" id="text" aria-describedby="name" placeholder="Nome">
          </div>

          <!-- input email -->
          <div class="form-group">
            <input name="Email" type="email" class="form-control" placeholder="E-mail*" required>
          </div>


          <!-- input telefone -->
          <div class="form-group">
            <input type="tel" name="Telefone" class="form-control js-input-celular" id="tel2" aria-describedby="tel" required>

          </div>


          <!-- input checkbox -->
          <p>
            <input name="Termos" type="checkbox" value="aceito" required>Li e aceito os termos e
            <a data-toggle="modal" data-target="#modalpolitica">Política de Privacidade</a>.
          </p>

          <!-- botão enviar -->
          <div class="submit-button">
            <button id="form-botao-<?= $formName ?>" type="submit" class="">Solicitar mais informações</button>
          </div>

          <div class="text-center submit-message font-weight-bold mt-3"></div>

        </form>
        <?php unset($formName); ?>
        <?php ############################################################################################################# 
        ?>
        <a href="./assets/pdf/BOOK_INSPIRA.pdf" download>
          <div class="book-container">
            <p>Veja o book de vendas completo</p>
          </div>
        </a>
        <a data-fslightbox="html5-youtube-videos" href="https://www.youtube.com/watch?v=8K1C_EXjAWo" class="video-link">
          <div class="video-container">
            <i class="fas fa-video"></i>
            <p>Assista a nossa Inspiração</p>
          </div>
        </a>
        <div class="last-section">
          <p>Realização: Prodomo e Leblon Realty</p>
        </div>
      </div>
      <div class="last-section-mobile">
        <p>Realização:<br> Prodomo e Leblon Realty</p>
      </div>
    </div>
    <!-- BOTAO WHATSAPP -->
    <div id="barrainfo">
      <a class="shake2" data-toggle="modal" data-target="#modalWhatsApp" id="modalWppLink">
        <img src="./assets/src/img/whatsapp.svg" alt="Botão para formulário de whatsapp">
      </a>
    </div>
    <!-- MODAL WHATSAPP -->
    <div class="modal fade modalWhatsApp" id="modalWhatsApp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form class="js-form" id="form-whatsapp">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">WhatsApp</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Coloque seu nome, número de celular e e-mail para falar conosco por WhatsApp agora.</p>
              <div class="form-container">
                <div class="form-wrapper">
                  <?php ############################################################################################################# 
                  ?>
                  <?php $formName = 'whatsapp'; ?>
                  <form action="" name="<?= $formName ?>" id="form-<?= $formName ?>">

                    <!-- input nome -->
                    <div class="form-group">
                      <label for="Nome" class="text-uppercase">Nome completo</label>
                      <input name="Nome" type="text" class="p-2" placeholder="Digite seu nome*" required>
                    </div>

                    <!-- input email -->
                    <div class="form-group">
                      <label for="Email" class="text-uppercase mt">E-mail</label>
                      <input name="Email" type="email" class="p-2" placeholder="Digite seu e-mail*" required>
                    </div>

                    <!-- input telefone -->
                    <div class="form-group">
                      <label for="tel" class="text-uppercase mt">Telefone</label>
                      <input type="tel" name="Telefone" class="form-control js-input-celular" id="tel" aria-describedby="tel" required>
                    </div>

                    <!-- input checkbox -->
                    <div class="form-check">
                      <input name="Termos" class="form-check-input" type="checkbox" id="Check" value="aceito" required>
                      <label class="form-check-label text-uppercase" for="Check">
                        Eu li e aceito os termos e
                        <a data-toggle="modal" data-target="#modalpolitica">Política de Privacidade</a>.
                      </label>
                    </div>

                    <!-- botão enviar -->
                    <button id="form-botao-<?= $formName ?>" type="submit" class="text-uppercase">Solicitar mais informações</button>

                    <div class="text-center submit-message font-weight-bold mt-3"></div>

                  </form>
                  <?php unset($formName); ?>
                  <?php ############################################################################################################# 
                  ?>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
    <!-- FIM MODAL WHATSAPP -->
  </section>




  <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(página atual)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Ação</a>
            <a class="dropdown-item" href="#">Outra ação</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Algo mais aqui</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Desativado</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>
      </form>
    </div>
  </nav> -->
</main>





<?php

require './footer.php';

?>