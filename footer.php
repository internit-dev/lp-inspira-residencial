<footer>
</footer>


  <!-- LIBARY's -->
  <script src="./assets/dist/js/libary/bootstrap/bootstrap.min.js"></script>
  <script src="./assets/dist/js/libary/animations/web-animations.js"></script>
  <script src="./assets/dist/js/libary/lightbox/fslightbox.min.js"></script>
  <script src="./assets/dist/js/libary/mask/jquery.mask.min.js"></script>
  <script src="./assets/dist/js/libary/tellinput/intlTelInput.min.js"></script>
  <script src="./assets/dist/js/libary/swiper/swiper-bundle.min.js"></script>
  <script src="./assets/dist/js/libary/storage/storage.js"></script>

  <!-- SCRIPTS -->
  <script type="module" src="./assets/dist/js/template-globals.js"></script>
  <script src="./assets/dist/js/main/form/_form.js"></script>

  <cookie>
		<div class="cookie-content">
			<div class="cookie-content-text">
			<p>Para melhorar a sua experiência de navegação, utilizamos de cookies, entre outras tecnologias.
			De acordo com a nossa <a href="./politica-de-privacidade">Política de Privacidade</a>, ao continuar navegando, você aceita estas condições.
			Acesse nossa <a href="./politica-de-privacidade.php">Política de Privacidade e confira como tratamos os dados pessoais</a>.</p>
			</div>
			<div class="btn-confirm">
				<button data-cookie="sim">Concordar e continuar</button>
			</div>
		</div>
	</cookie>

  <!-- MODAL POLÍTICA DE PRIVACIDADE -->
<div class="modal fade modal-politica" id="modalpolitica" aria-hidden="true" aria-labelledby="modalPoliticaPrivacidadeToggle" tabindex="-1">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-uppercase" id="modalPoliticaPrivacidadeToggle">Política de Privacidade</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="politica">
          <div class="">
            <div class="text">
              <p>Este site é mantido e operado por AM147 Empreendimento Imobiliário SPE S/A</p>
              <p>CNPJ: 35.134.435/0001-03</p>
            </div>
            <div class="text">
              <p>Nós coletamos e utilizamos alguns dados pessoais que pertencem àqueles que utilizam nosso site. Ao fazê-lo, agimos na qualidade de controlador desses dados e estamos sujeitos às disposições da Lei Federal n. 13.709/2018 (Lei Geral de Proteção de Dados Pessoais LGPD).</p>
            </div>
            <div class="text">
              <p>Nós cuidamos da proteção de seus dados pessoais e, por isso, disponibilizamos esta política de privacidade, que contém informações importantes sobre: </p>
            </div>
            <div class="list-type-one">
              <ul>
                <li>
                  <p>Quem deve utilizar nosso site </p>
                </li>
                <li>
                  <p>Quais dados coletamos e o que fazemos com eles; </p>
                </li>
                <li>
                  <p>Seus direitos em relação aos seus dados pessoais; e </p>
                </li>
                <li>
                  <p>Como entrar em contato conosco.</p>
                </li>
              </ul>
            </div>
            <div class="indice my-5">
              <h6><strong>1. Dados que coletamos e motivos da coleta</strong></h6>
            </div>
            <div class="text">
              <p>Nosso site coleta e utiliza alguns dados pessoais de nossos usuários, de acordo com o disposto nesta seção.</p>
            </div>
            <div class="subtitulo mb-5">
              <h6><i>1. Dados pessoais fornecidos expressamente pelo usuário</i></h6>
            </div>
            <div class="text">
              <p>Nós coletamos os seguintes dados pessoais que nossos usuários nos fornecem expressamente ao utilizar nosso site:</p>
            </div>
            <div class="list-type-one">
              <span>Formulários de Contato do Site:</span>
              <ul>
                <li>
                  <p>Nome</p>
                </li>
                <li>
                  <p>E-mail;</p>
                </li>
                <li>
                  <p>DDD + Telefone;</p>
                </li>
                <li>
                  <p>DDD + Celular;</p>
                </li>
              </ul>
            </div>
            <div class="text">
              <p>A coleta destes dados ocorre nos seguintes momentos:</p>
            </div>
            <div class="list-type-one">
              <ul>
                <li>
                  <p>Solicitação de contato através do formulário de "Fale Conosco";</p>
                </li>
                <li>
                  <p>Solicitação de contato através do formulário "Trabalhe Conosco";</p>
                </li>
                <li>
                  <p>Solicitação de contato através do formulário de "WhatsApp";</p>
                </li>
                <li>
                  <p>Solicitação de contato através do formulário "Contato por E-mail";</p>
                </li>
                <li>
                  <p>Solicitação de contato através do formulário "Ofereça seu Terreno"; </p>
                </li>
              </ul>
            </div>
            <div class="text">
              <p>Os dados fornecidos por nossos usuários são coletados com as seguintes finalidades:</p>
            </div>
            <div class="list-type-one">
              <ul>
                <li>
                  <p>Para que o usuário possa receber mais informações sobre um ou mais imóveis específicos e também receber novas ofertas de novas unidades de imóveis. </p>
                </li>
                <li>
                  <p>Para o esclarecimento de alguma dúvida, fazer um elogio ou reclamação.</p>
                </li>
              </ul>
            </div>
            <div class="subtitulo">
              <p><i>2. Dados pessoais obtidos de outras formas</i></p>
            </div>
            <div class="text">
              <p>Nós coletamos os seguintes dados pessoais de nossos usuários: </p>
            </div>
            <div class="list-type-one">
              <ul>
                <li>
                  <p>Endereço IP</p>
                </li>
                <li>
                  <p>Dados de Geolocalização</p>
                </li>
                <li>
                  <p>Histórico de navegação e fluxo de acesso ao site.</p>
                </li>
              </ul>
            </div>
            <div class="list-type-one">
              <span>A coleta destes dados ocorre nos seguintes momentos:</span>
              <ul>
                <li>
                  <p>Ao acessar o site e aceitar os Cookies;</p>
                </li>
                <li>
                  <p>Ao navegar nas páginas do site;</p>
                </li>
                <li>
                  <p>Ao fazer login em "Portal do Cliente";</p>
                </li>
                <li>
                  <p>Ao salvar imóveis em seus Favoritos;</p>
                </li>
              </ul>
            </div>
            <div class="list-type-one">
              <span>Estes dados são coletados com as seguintes finalidades:</span>
              <ul>
                <li>
                  <p>Personalizar a experiência do usuário; </p>
                </li>
                <li>
                  <p>Parametrizar opções de ofertas de imóveis; </p>
                </li>
                <li>
                  <p>Mapear o uso do site; </p>
                </li>
                <li>
                  <p>Analisar a aceitação do conteúdo ofertado.</p>
                </li>
              </ul>
            </div>
            <div class="subtitulo">
              <p><i>3. Dados sensíveis</i></p>
            </div>
            <div class="text">
              <p><strong>Não</strong> serão coletados dados sensíveis de nossos usuários, assim entendidos aqueles definidos nos arts. 11 e seguintes da Lei de Proteção de Dados Pessoais. Assim, <strong>não</strong> haverá coleta de dados sobre origem racial ou étnica, convicção religiosa, opinião política, filiação a sindicato ou a organização de caráter religioso, filosófico ou político, dado referente à saúde ou à vida sexual, dado genético ou biométrico, quando vinculado a uma pessoa natural.
              </p>
            </div>
            <div class="subtitulo">
              <p><i>4. Cookies</i></p>
            </div>
            <div class="text">
              <p>Cookies são pequenos arquivos de texto baixados automaticamente em seu dispositivo quando você acessa e navega por um site. Eles servem, basicamente, para que seja possível identificar dispositivos, atividades e preferências de usuários.</p>
            </div>
            <div class="text">
              <p>Os cookies não permitem que qualquer arquivo ou informação sejam extraídos do disco rígido do usuário, não sendo possível, ainda, que, por meio deles, se tenha acesso a informações pessoais que não tenham partido do usuário ou da forma como utiliza os recursos do site.</p>
            </div>
            <div class="subelement-list">
              <p><i>a. Cookies do site</i></p>
            </div>
            <div class="text">
              <p>Os cookies do site são aqueles enviados ao computador ou dispositivo do usuário e administrador exclusivamente pelo site.</p>
            </div>
            <div class="text">
              <p>As informações coletadas por meio destes cookies são utilizadas para melhorar e personalizar a experiência do usuário, sendo que alguns cookies podem, por exemplo, ser utilizados para lembrar as preferências e escolhas do usuário, bem como para o oferecimento de conteúdo personalizado.</p>
            </div>
            <div class="subelement-list">
              <p><i>b. Cookies de terceiros</i></p>
            </div>
            <div class="text">
              <p>Alguns de nossos parceiros podem configurar cookies nos dispositivos dos usuários que acessam nosso site.</p>
            </div>
            <div class="text">
              <p>Estes cookies, em geral, visam possibilitar que nossos parceiros possam oferecer seu conteúdo e seus serviços ao usuário que acessa nosso site de forma personalizada, por meio da obtenção de dados de navegação extraídos a partir de sua interação com o site.</p>
            </div>
            <div class="text">
              <p>O usuário poderá obter mais informações sobre os cookies de terceiro e sobre a forma como os dados obtidos a partir dele são tratados, além de ter acesso à descrição dos cookies utilizados e de suas características, acessando o seguinte link:</p>
            </div>
            <div class="list-type-one">
              <ul>
                <li>
                  <p>Google Ads e Analytics: https://static.googleusercontent.com/media/www.google.com/pt-BR//intl/pt-BR/policies/privacy/google_privacy_policy_pt-BR.pdf</p>
                </li>
                <li>
                  <p>Facebook: https://www.facebook.com/policy.php </p>
                </li>
                <li>
                  <p>Instagram: https://www.instagram.com/terms/accept/?hl=pt-br
                    https://www.facebook.com/help/instagram/519522125107875 </p>
                </li>
                <li>
                  <p>RD Station: https://www.rdstation.com/pt/politica-de-privacidade/</p>
                </li>
              </ul>
            </div>
            <div class="text">
              <p>As entidades encarregadas da coleta dos cookies poderão ceder as informações obtidas a terceiros.</p>
            </div>
            <div class="subelement-list">
              <p><i>c. Gestão de cookies</i></p>
            </div>
            <div class="text">
              <p>O usuário poderá se opor à utilização de cookies pelo site, bastando que os desative no momento em que começa a utilizar o serviço, seguindo as seguintes instruções:</p>
            </div>
            <div class="subelement">
              <p>Assim que entrar no site, o usuário terá a opção de "Aceitar e continuar" a utilização de cookies, bastando que selecione a opção correspondente na caixa de diálogo (pop up) carregada automaticamente assim que nossa página é acessada.</p>
            </div>
            <div class="text">
              <p>A desativação de todos os cookies, no entanto, não será possível, uma vez que alguns deles são essenciais para que o site funcione corretamente.</p>
            </div>
            <div class="text">
              <p>Além disso, a desativação dos cookies que podem ser desabilitados poderá prejudicar a experiência do usuário, uma vez que informações utilizadas para personalizá-la deixarão de ser utilizadas.</p>
            </div>
            <div class="subtitulo">
              <p><i>5. Coleta de dados não previstos expressamente</i></p>
            </div>
            <div class="text">
              <p>Eventualmente, outros tipos de dados não previstos expressamente nesta Política de Privacidade poderão ser coletados, desde que sejam fornecidos com o consentimento do usuário, ou, ainda, que a coleta seja permitida com fundamento em outra base legal prevista em lei.</p>
            </div>
            <div class="text">
              <p>Em qualquer caso, a coleta de dados e as atividades de tratamento dela decorrentes serão informadas aos usuários do site.</p>
            </div>
            <div class="indice my-5">
              <h5><strong>2. Compartilhamento de dados pessoais com terceiros</strong></h5>
            </div>
            <div class="text">
              <p>Nós não compartilhamos seus dados pessoais com terceiros. Apesar disso, é possível que o façamos para cumprir alguma determinação legal ou regulatória, ou, ainda, para cumprir alguma ordem expedida por autoridade pública.</p>
            </div>
            <div class="indice my-5">
              <h5><strong>3. Por quanto tempo seus dados pessoais serão armazenados</strong></h5>
            </div>
            <div class="text">
              <p>Os dados pessoais que coletamos serão armazenados e utilizados pelos seguintes períodos de tempo:</p>
            </div>
            <div class="list-type-one">
              <ul>
                <li>
                  <p>Os dados de login em "Portal do Cliente" serão armazenados enquanto o usuário estiver com a conta ativa, podendo a qualquer momento requerer a remoção de sua conta e acesso. </p>
                </li>
                <li>
                  <p>Os dados coletados em formulários de interesse em imóveis, permanecerão armazenados sob nossos cuidados e em segurança até o momento que o usuário solicitar ou o mesmo fazer a remoção, pois entenderemos que, enquanto não solicitar a sua remoção, o mesmo ainda terá o interesse de receber mais informações sobre imóveis, para os quais ele solicitou e outros que julgamos como fazer parte de seu interesse.</p>
                </li>
              </ul>
            </div>
            <div class="text">
              <p>Os períodos informados não são superiores ao estritamente necessário, atendendo às finalidades e às justificativas legais para o tratamento dos dados.</p>
            </div>
            <div class="text">
              <p>Vale dizer que, se houver alguma justificativa legal ou regulatória, os dados poderão continuar armazenados ainda que a finalidade para a qual foram coletados ou tenham sido tratados tenha se exaurido.</p>
            </div>
            <div class="text">
              <p>Uma vez finalizado o tratamento, observadas as disposições desta seção, os dados são apagados ou anonimizados.</p>
            </div>
            <div class="indice my-5">
              <h5><strong>4. Bases legais para o tratamento de dados pessoais</strong></h5>
            </div>
            <div class="text">
              <p>Uma base legal para o tratamento de dados pessoais nada mais é que um fundamento jurídico, previsto em lei, que o justifica. Assim, cada operação de tratamento de dados pessoais precisa ter uma base legal a ela correspondente.</p>
            </div>
            <div class="list-type-one">
              <span>Nós tratamos os dados pessoais de nossos usuários nas seguintes hipóteses:</span>
              <ul>
                <li>
                  <p>Mediante o consentimento do titular dos dados pessoais</p>
                </li>
                <li>
                  <p>Quando necessário para atender aos interesses legítimos do controlador ou de terceiro</p>
                </li>
              </ul>
            </div>
            <div class="subtitulo">
              <p><i>1. Consentimento</i></p>
            </div>
            <div class="text">
              <p>Determinadas operações de tratamento de dados pessoais realizadas em nosso site dependerão da prévia concordância do usuário, que deverá manifestá-la de forma livre, informada e inequívoca.</p>
            </div>
            <div class="text">
              <p>O usuário poderá revogar seu consentimento a qualquer momento, sendo que, não havendo hipótese legal que permita ou que demande o armazenamento dos dados, os dados fornecidos mediante consentimento serão excluídos.</p>
            </div>
            <div class="text">
              <p>Além disso, se desejar, o usuário poderá não concordar com alguma operação de tratamento de dados pessoais baseada no consentimento. Nestes casos, porém, é possível que não possa utilizar alguma funcionalidade do site que dependa daquela operação. As consequências da falta de consentimento para uma atividade específica são informadas previamente ao tratamento.</p>
            </div>
            <div class="subtitulo">
              <p><i>2. Legítimo interesse</i></p>
            </div>
            <div class="text">
              <p>Para determinadas operações de tratamento de dados pessoais, nos baseamos exclusivamente em nosso interesse legítimo. Para saber mais sobre em quais casos, especificamente, nos valemos desta base legal, ou para obter mais informações sobre os testes que fazemos para termos certeza de que podemos utilizá-la, entre em contato com nosso Encarregado de Proteção de Dados Pessoais por algum dos canais informados nesta Política de Privacidade, na seção "Como entrar em contato conosco".</p>
            </div>
            <div class="indice">
              <h5><strong>5. Direitos do usuário</strong></h5>
            </div>
            <div class="list-type-one">
              <span>O usuário do site possui os seguintes direitos, conferidos pela Lei de Proteção de Dados Pessoais:</span>
              <ul>
                <li>
                  <p>Confirmação da existência de tratamento; </p>
                </li>
                <li>
                  <p>Acesso aos dados; </p>
                </li>
                <li>
                  <p>Correção de dados incompletos, inexatos ou desatualizados; </p>
                </li>
                <li>
                  <p>Anonimização, bloqueio ou eliminação de dados desnecessários, excessivos ou tratados em desconformidade com o disposto na lei;</p>
                </li>
                <li>
                  <p>Portabilidade dos dados a outro fornecedor de serviço ou produto, mediante requisição expressa, de acordo com a regulamentação da autoridade nacional, observados os segredos comercial e industrial;</p>
                </li>
                <li>
                  <p>Eliminação dos dados pessoais tratados com o consentimento do titular, exceto nos casos previstos em lei;</p>
                </li>
                <li>
                  <p>Informação das entidades públicas e privadas com as quais o controlador realizou uso compartilhado de dados;</p>
                </li>
                <li>
                  <p>Informação sobre a possibilidade de não fornecer consentimento e sobre as consequências da negativa;</p>
                </li>
                <li>
                  <p>Revogação do consentimento.</p>
                </li>
              </ul>
            </div>
            <div class="text">
              <p>É importante destacar que, nos termos da LGPD, não existe um direito de eliminação de dados tratados com fundamento em bases legais distintas do consentimento, a menos que os dados sejam desnecessários, excessivos ou tratados em desconformidade com o previsto na lei.</p>
            </div>
            <div class="subtitulo">
              <p><i>1. Como o titular pode exercer seus direitos</i></p>
            </div>
            <div class="text">
              <p>Os titulares de dados pessoais tratados por nós poderão exercer seus direitos por meio de pedido de acesso aos dados através do e-mail envioinspira@gmail.com. Alternativamente, se desejar, o titular poderá enviar um e-mail ou uma correspondência ao nosso Encarregado de Proteção de Dados Pessoais. As informações necessárias para isso estão na seção "Como entrar em contato conosco" desta Política de Privacidade.</p>
            </div>
            <div class="text">
              <p>Os titulares de dados pessoais tratados por nós poderão exercer seus direitos a partir do envio de mensagem ao nosso Encarregado de Proteção de Dados Pessoais, seja por e-mail ou por correspondência. As informações necessárias para isso estão na seção "Como entrar em contato conosco" desta Política de Privacidade.</p>
            </div>
            <div class="text">
              <p>Para garantir que o usuário que pretende exercer seus direitos é, de fato, o titular dos dados pessoais objeto da requisição, poderemos solicitar documentos ou outras informações que possam auxiliar em sua correta identificação, a fim de resguardar nossos direitos e os direitos de terceiros. Isto somente será feito, porém, se for absolutamente necessário, e o requerente receberá todas as informações relacionadas.
              </p>
            </div>
            <div class="indice my-5">
              <h5><strong>6. Medidas de segurança no tratamento de dados pessoais</strong></h5>
            </div>
            <div class="text">
              <p>Empregamos medidas técnicas e organizativas aptas a proteger os dados pessoais de acessos não autorizados e de situações de destruição, perda, extravio ou alteração desses dados.</p>
            </div>
            <div class="text">
              <p>As medidas que utilizamos levam em consideração a natureza dos dados, o contexto e a finalidade do tratamento, os riscos que uma eventual violação geraria para os direitos e liberdades do usuário, e os padrões atualmente empregados no mercado por empresas semelhantes à nossa.</p>
            </div>
            <div class="list-type-one">
              <span>Entre as medidas de segurança adotadas por nós, destacamos as seguintes:</span>
              <ul>
                <li>
                  <p>Armazenamento de senhas utilizando hashes criptográficos; </p>
                </li>
                <li>
                  <p>Restrições de acessos a bancos de dados;</p>
                </li>
                <li>
                  <p>Monitoramento de acesso físicos a servidores;</p>
                </li>
                <li>
                  <p>Segurança de ataques de acordo com as políticas da Amazon aws.</p>
                </li>
              </ul>
            </div>
            <div class="text">
              <p>Ainda que adote tudo o que está ao seu alcance para evitar incidentes de segurança, é possível que ocorra algum problema motivado exclusivamente por um terceiro como em caso de ataques de hackers ou crackers ou, ainda, em caso de culpa exclusiva do usuário, que ocorre, por exemplo, quando ele mesmo transfere seus dados a terceiro. Assim, embora sejamos, em geral, responsáveis pelos dados pessoais que tratamos, nos eximimos de responsabilidade caso ocorra uma situação excepcional como essas, sobre as quais não temos nenhum tipo de controle.</p>
            </div>
            <div class="text">
              <p>De qualquer forma, caso ocorra qualquer tipo de incidente de segurança que possa gerar risco ou dano relevante para qualquer de nossos usuários, comunicaremos os afetados e a Autoridade Nacional de Proteção de Dados acerca do ocorrido, em conformidade com o disposto na Lei Geral de Proteção de Dados.</p>
            </div>
            <div class="indice my-5">
              <h5><strong>7. Reclamação a uma autoridade de controle</strong></h5>
            </div>
            <div class="text">
              <p>Sem prejuízo de qualquer outra via de recurso administrativo ou judicial, os titulares de dados pessoais que se sentirem, de qualquer forma, lesados, podem apresentar reclamação à Autoridade Nacional de Proteção de Dados.</p>
            </div>
            <div class="indice my-5">
              <h5><strong>8. Alterações nesta política</strong></h5>
            </div>
            <div class="text">
              <p>A presente versão desta Política de Privacidade foi atualizada pela última vez em: 22/10/2020.</p>
            </div>
            <div class="text">
              <p>Nos reservamos o direito de modificar, a qualquer momento, as presentes normas, especialmente para adaptá-las às eventuais alterações feitas em nosso site, seja pela disponibilização de novas funcionalidades, seja pela supressão ou modificação daquelas já existentes.</p>
            </div>
            <div class="text">
              <p>Sempre que houver uma modificação, nossos usuários serão notificados acerca da mudança.</p>
            </div>
            <div class="indice my-5">
              <h5><strong>9. Como entrar em contato conosco</strong></h5>
            </div>
            <div class="text">
              <p>Para esclarecer quaisquer dúvidas sobre esta Política de Privacidade ou sobre os dados pessoais que tratamos, entre em contato com nosso Encarregado de Proteção de Dados Pessoais, por algum dos canais mencionados abaixo:</p>
            </div>
            <div class="subelement-list">
              <p><i>E-mail: envioinspira@gmail.com</i></p>
            </div>
            <div class="subelement-list">
              <p>Endereço postal: Av. João Cabral de Mello Neto, 850 Bloco 3 Sala 1002 – Barra da Tijuca - Rio de Janeiro - RJ.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- FIM MODAL POLÍTICA DE PRIVACIDADE -->



  <!-- INTEGRAÇÃO FOOTER -->
  <?php require_once 'integracao/integracao.php'; ?>

</body>
</html>