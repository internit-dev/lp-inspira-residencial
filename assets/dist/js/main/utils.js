const swiperDesktop = document.getElementById('swiperDesktop')
const swiperMobile = document.getElementById('SwiperMobile')

if(window.innerWidth < 867) {
    swiperMobile.classList.add('swiper-pagination');
    swiperDesktop.classList.remove('swiper-pagination');
}